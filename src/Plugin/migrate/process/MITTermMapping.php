<?php

namespace Drupal\findit_mit_sync\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 *
 * @MigrateProcessPlugin(
 *   id = "findit_mit_term_mapping"
 * )
 */
class MITTermMapping extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $term_mapping = $row->get($this->configuration['term_mapping']);
    $skip_list = $row->get($this->configuration['skip_list']);
    $raw_filters_interest = $row->get($this->configuration['filters_interest']);
    $filters_interest = is_array($raw_filters_interest) ? array_column($raw_filters_interest, 'name') : [];
    $raw_filters_type = $row->get($this->configuration['filters_type']);
    $filters_type = is_array($raw_filters_type) ? array_column($raw_filters_type, 'name') : [];
    $keywords = $row->get($this->configuration['keywords']);
    $tags = $row->get($this->configuration['tags']);

    if (!empty($skip_list['keywords']) && array_intersect($keywords, $skip_list['keywords'])) {
      $library_event_id = $row->get('src_unique_id');
      $flat_keywords_list = implode(', ', $skip_list['keywords']);
      $message = "MIT event with ID '$library_event_id' includes one of the excluded keywords: $flat_keywords_list. Skipping import operation.";
      throw new MigrateSkipRowException($message);
    }

    if (!empty($skip_list['tags']) && array_intersect($tags, $skip_list['tags'])) {
      $library_event_id = $row->get('src_unique_id');
      $flat_tags_list = implode(', ', $skip_list['tags']);
      $message = "MIT event with ID '$library_event_id' includes one of the excluded tags: $flat_tags_list. Skipping import operation.";
      throw new MigrateSkipRowException($message);
    }

    if (!empty($skip_list['filters']['interest']) && array_intersect($filters_interest, $skip_list['filters']['interest'])) {
      $library_event_id = $row->get('src_unique_id');
      $flat_interest_list = implode(', ', $skip_list['filters']['interest']);
      $message = "MIT event with ID '$library_event_id' includes one of the excluded interest filters: $flat_interest_list. Skipping import operation.";
      throw new MigrateSkipRowException($message);
    }

    if (!empty($skip_list['filters']['type']) && array_intersect($filters_type, $skip_list['filters']['type'])) {
      $library_event_id = $row->get('src_unique_id');
      $flat_type_list = implode(', ', $skip_list['filters']['type']);
      $message = "MIT event with ID '$library_event_id' includes one of the excluded type filters: $flat_type_list. Skipping import operation.";
      throw new MigrateSkipRowException($message);
    }

    $activities_tids = [];
    foreach ($term_mapping as $term_id => $mapping) {
      if (array_intersect($filters_interest, $mapping['interest'])) {
        $activities_tids[] = $term_id;
      }
      if (array_intersect($filters_type, $mapping['type'])) {
        $activities_tids[] = $term_id;
      }
    }

    return array_unique($activities_tids);
  }
}
