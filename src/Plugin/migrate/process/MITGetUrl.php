<?php

namespace Drupal\findit_mit_sync\Plugin\migrate\process;

use Drupal\Core\Url;
use Drupal\Component\Utility\UrlHelper;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 *
 * @MigrateProcessPlugin(
 *   id = "findit_mit_get_url"
 * )
 */
class MITGetUrl extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $url = UrlHelper::stripDangerousProtocols($value);

    if (substr( $url, 0, 4 ) === "http" && filter_var($url, FILTER_VALIDATE_URL)) {
      return Url::fromUri($url)->toString();
    }

    if (filter_var($url, FILTER_VALIDATE_EMAIL)) {
      return "";
    }

    if (filter_var("https://$url", FILTER_VALIDATE_URL)) {
      return Url::fromUri("https://$url")->toString();
    }

    return "";
  }

}
